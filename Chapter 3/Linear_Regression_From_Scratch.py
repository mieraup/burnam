from d2l import mxnet as d2l
from mxnet import autograd, np, npx
import random

#Here we generate the sythetic data
def synthetic_data(w, b, num_examples): #@save
    """Generate y=Xw + b + noise"""
    X = np.random.normal(0, 1, (num_examples, len(w)))
    #We are taking alot of samples with some noise
    y = np.dot(X, w) + b
    y += np.random.normal(0, 0.01, y.shape)
    return X, y.reshape((-1, 1))  #We are returnign data tuple that has the [value1, value2] and y
    #We are resphaping so there is 1 data in second axie and let first axis manage itself
    # X is a matrix of num_examples * 2 dimension

true_w = np.array([2, -3.4])
true_b = 4.2  #This is the bias

features, labels = synthetic_data(true_w, true_b, 1000)

print('features:', features[0], '\nlabel:', labels[0])
#START Need to be in juypter notebook for this to work START
# d2l.set_figsize()
# d2l.plt.scatter(d2l.numpy(features[:, 1]), d2l.numpy(labels), 1)
#END Need to be in juypter notebook for this to work END


#Now we are going to read the dataset

def data_iter(batch_size, features, labels):
    num_examples = len(features)
    indices = list(range(num_examples))  #Generates a list of numbers from 0 up to num_examples and returns an iterable list
    #NOTE this is NOT very efficient for handling large lists
    random.shuffle(indices)  #Alright this is beautiful, instead of actually shuffling the list of actual data (which could be costly for higher dimensional tensors) we shuffle a list of numbers that represent each item in the list.
    for i in range(0, num_examples, batch_size):  #We are taking batch size steps thru the list of shuffled indexes
        batch_indices = np.array(
            indices[i: min(i + batch_size, num_examples)])  #We using ILOC (Integer Based Location)
        yield features[batch_indices], labels[batch_indices]  # We are using yield which preserves the local variables when the function  returns
        #this is useful for generators where data is generated AS needed. This also makes the code faster since we only want to genreate what we are going to consume RIGHT away. Not what we will consume next cycle
        #The batch_indicies selects the correct indexes from the num_examples and batch_size and returns them to the caller.

#START uncomment to see what data looks like START
# batch_size = 10
# for X, y in data_iter(batch_size, features, labels):
#     print(X, '\n', y)
#     break  #Just so that we can see the data
#END uncomment to see what data looks like END

#Didactic purposes means "Intending for teaching"
#This is what synthetic_data and data_iter are doing. The are poor implementations designed to teach the underlying suystem so that I understand how the system works. Once I do then I'll use libraries built by very smart people.

#Model parameters

w = np.random.normal(0, 0.01, (2, 1))  #Creating two neurons initilized to random values for the weights
b = np.zeros(1)
w.attach_grad()
b.attach_grad()  #This attaches the a "magic feature" that automatically computes the gradients for us so that we can do what we need to with the gradient.
#Gradient computation is hard especially for multi dimensional systems
#We want to see the "best direction" to go to increase fastest

#Defining the model
def linreg(X, w, b):  #@save
    """The linear regression model."""
    return np.dot(X, w) + b  #We are updating all parameters by the guessed weight in a single swoop. Because vectorization is being used, we are using BLAS code most likely that is optimized directly for the hardware and possibly even the cache layout to maximize computation

def squared_loss(y_hat, y):  #@save
    """Squared Loss.""" #Question that I have is why must we every single time make sure that y is matched to yhat beforing undertaking the loss function
    return (y_hat - y.reshape(y_hat.shape)) ** 2 / 2

def sgd(params, lr, batch_size):  #@save
    for param in params:
        param[:] = param - lr * param.grad / batch_size  #This last part normalizes movement amount so movement is NOT AS influenced by batch size
        
#Remeber the main goals:
# 1 - Intilize parameters (vector of weights + bias),
# 2 - Compute gradient and update parameters
# Repeat until training complete

#For this example we have the epoch hyperparmeter set to 3 and the learning rate hyperparameter set to 0.03 these are found by trial and error (for now)

lr =0.03
num_epochs = 3
net = linreg
loss = squared_loss
batch_size = 10
for epoch in range(num_epochs):
    for X, y in data_iter(batch_size, features, labels):
        with autograd.record():
            l = loss(net(X, w, b), y)  #Calc loass in 'X' and 'y'
        #Because 'l' has a shape of ('batch_size',1) and NOT scalar
        #variable, the elementsin 'l' are added togetaer to objtain a new varialbe on which gradients w/ respect to ['w'.'b'] are calculated
        l.backward()
        sgd([w, b], lr, batch_size)  #update params with graidents
    train_l = loss(net(features, w, b), labels)
    print(f'epoch {epoch + 1}, loss {float(train_l.mean()): f}')
print(f'error in estimated w: {true_w - w.reshape(true_w.shape)}') #Why do we keep calling reshape?
print(f'error in estimating b: {true_b - b}')

#Remember goal is to recover parameters "close enogh" to get the answers that we want.
#Deep networks have many possible configurations that would lead to the same answer.