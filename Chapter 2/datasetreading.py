import os 
#The #@save is a special mark where the following, class, satement or functions are saved in the d2l package so it can be referend later as d2l.whateveritiscalled
def mkdir_if_not_exist(path):  #@save
        """Make a directory if it does not exist."""
        if not isinstance(path, str):
            path = os.path.join(*path)
        if not os.path.exists(path):
            os.makedirs(path)

#Here we are creating the raw dataset

data_file = "data/house_tiny.cvs"
mkdir_if_not_exist("data")
with open(data_file, 'w') as f:
        f.write("NumRooms, Alley, Price\n")  #Column names
        f.write("NA,Pave,127500\n")  #Datapoint
        f.write("2,NA,106000\n")
        f.write("4,NA,178100\n") #Cannot have spaces for NA. NA means NaN or does not exist. 
        f.write("NA,NA,140000\n")


import pandas as pd

data = pd.read_csv(data_file)
print(data)

#When we are reading data, we need to correct missing values there are two ways of doing this

# 1) Imputation - Replace missing values with substitution
# 2) Deletion - Ignore missing values. We used deletion right about on line 25 when printing the data

# We will demonstrate imputation right below

#ILOC stands for integer location based indexing.
#For inputs we are reading, all the rows for the first two columns
#For outputs we are reading the last colum and all the rows for that last column

inputs, outputs = data.iloc[:, 0:2], data.iloc[:, 2]

inputs = inputs.fillna(inputs.mean())  #We are filling missing values with the mean of the rest of the values in that particular column
print(inputs)

#For data that is categorical/discreate,  IE Integers than we can expand the the categories into a series of catogies that have a boolean representation for if that category is the right category
# That is we can have the dataset columns be done as NumRooms, Ally_Pave, Ally_NA, Price
#                                                       NA      1           0   127500
#                                                       2       0           1   106000
#                                                       4       0           1   178100
#                                                       NA      0           1   140000

inputs = pd.get_dummies(inputs, dummy_na=True)
print(inputs)