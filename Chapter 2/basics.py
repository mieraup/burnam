from mxnet import np, npx
# NP is numpy
# npx is num py extensions
npx.set_np()

#Tensor repseents a multi or single dimensiona array of values. With single dimension can be viewed as a vector, with two dimensions can be viewed as a matrix, with more than two, no specfic term.

x = np.arange(12)  #Each item is  an element of the tensor
#Goes from 0 - 12 (exclusive) as floats

print(x)

#We can access the tensors shape by  calling .shape

print("Tensors Shape is :", x.shape)

#Want total number of elements, we can call .size, for a vector tensor, there is no difference, but for multi dimensional tensor, there will be a difference

print("Tensor Size is :", x.size)

#We will now do some more fun stuff

print('"Hold on, this is where the fun begins" - Anakin Skywalker')

#We can reshape from a vector to a matrix or even a multi dimensional tensor

x=x.reshape(3, 4)

print("Reshaped to 3x4 matrix\n", x)

x=x.reshape(2, 3, 2)

print("Reshaped to 2x2x2 tensor\n", x)

#If we want the code to automatically infer the missing dimension, then we can do -1

x=x.reshape(3,-1)

print("Reshaping to a 3x4 matrix infering 1 dimension\n", x)

#x = x.reshape(2, -1, -1)

print("Attempting to infer two dimensions for a 3 dimensional shape\n")
print("Two unknown infering FAILED")

print("Initilizing Tensors with all zeros in the size, 2x3x4")
x = np.zeros((2, 3, 4))
print(x)

print("Initilizing Tensors withy all ones in the size, 2x3x4")
x = np.ones((2, 3, 4))
print(x)

print("Intilizing with random vailues, with values randomly sampled for some probabiliy distributionb")
x = np.random.normal(0, 1, size=(3, 4)) #We are creating a tensor of size 3x4, with a normal gausian distribution with mean 0 and standard deviation of 1.
print(x)

print("We can also manually set each value for the tensor manually")
x = np.array([[2, 1, 4, 3], [1, 2, 3, 4,], [4, 3, 2, 1]])
print(x)
print("Expect that x shape is 3,4 :\n\t")
print(x.shape)
print("Expect that x size is 12 :\n\t")
print(x.size)

#We can perform scalar operations on each element, IE adding 5 to each iement num ber
