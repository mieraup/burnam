If we want the program to be right the first time ML is not the way to go. Methodical programming is the way to go.

If we want to answer questions that are difficult, ever changing or computationally intensive, then ML is the way to go.

Parts of ML training

- Dataset containing examples and some without the example
- Design a flexible program with parameters which tweak themselves to match our goal. Once we have fixed parameters then we have a 
- Model (we train by applying data and then tweaking knobs till output matches)

There is deep learning and machine learning.

Deep Learning is a subset of machine learning that uses a structure similar to the humane brain to solve problems

Machine Learning is a subset of AI that focuses on learning and adapting

AI is the encompasing science, study for making computer do something without being explicity programmed to do,


4 Terms that will follow us forever now

Dataset - What we train system with
Model - How to transform data
Loss Fuction - How bad our model is
Algorithm - Minimize loss function


ML can handle varying length data but fixed lenght data allows it to perform even better since that is one less variable that we need to wrry about.

We need to be able to objectively measure the AI's performance. We do that with a loss function, convention dictates that lower is better. However if someone has implic,ented that "higher is better", we can define a new function f prime = - f and that solves the problem.

The loss function can be applied in two areas:

training error - mistakes while learning

test error - mistakes while performing. 

Note it is very possible that the AI have close to 100% accuracy in training and close to 50% in test. This can occur when the AI has gotten so good on test data. We call this "overfitting the data".



Kinds of machine Learning

1 - Supervised learning
Has input and target denoted as example or instance.

There can be n instances of such thing.

Works as such

Training Inputs -> Supervised Learning <- Training Labels
                    ▼
Input ->           Model -> Output



Right now I'm interested in creating a system that can perform research with a human counterpart, providing research data and augmenting what the researcher is attempting to research.

The fundamental difference is that unlike a steam engine and coal, the data can be reused and reanalized in different ways, teasing out information again and again and again.