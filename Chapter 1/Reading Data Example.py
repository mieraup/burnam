data_file = 'test.cvs'
with open(data_file, 'w') as f:
    f.write("NumRooms,Price,Levels,Basements,Toilets,Ceilings,Wolves,Love\n")
    f.write("3,3,3,3,3,3,3,NA\n")
    f.write("4,4,4,4,4,4,4,NA\n")
    f.write("5,5,5,5,5,5,5,NA\n")
    f.write("6,6,6,6,6,6,6,6\n")
    f.write("NA,NA,3,NA,NA,NA,NA,NA\n")
    f.write("7,7,7,7,7,7,7,NA\n")
    f.write("NA,NA,NA,3,4,5,6,NA\n")
import pandas as pd

data = pd.read_csv(data_file)

rows = len(data)
listOfNullsPerRow = data.isna().sum(axis=1).tolist()
print(listOfNullsPerRow)
dropIndex = 0
maxNulls = listOfNullsPerRow[0]
for i in range(2, len(listOfNullsPerRow)):
    if (listOfNullsPerRow[i] > maxNulls):
        dropIndex = i;
        maxNulls = listOfNullsPerRow[dropIndex]

data = data.drop(dropIndex)  #Because data does not modify in place.

print(data)

from mxnet import np

tensor = np.array(data.values)

print(tensor.shape)

#Here is what we needed to do

# 1 Determine size of data
# 2 Loop thru the list of nulls per row, to prevent special cases, assume that the first index (0) has maxNulls of whatever that first index does. Then start looping from second onward. Only thing that we are missing is if the dat ahas 0 rows. But that's an edge case
# 3 Find largest value
# 4 Drop that index
# 5 Convert remaining data to tensor format.